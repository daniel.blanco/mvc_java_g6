package com.retos.reto1.service;

import com.retos.reto1.model.UserData;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class UserServices {

    public ResponseEntity<UserData> getData(String documentType, String documentNumber){

        //Si se manda un número en tipo de documento, error 500
        if(isValidDocumentNumber(documentType)){
            return ResponseEntity.internalServerError().body(null);
        }

        //Si se manda una letra que no sea P o C, error 400
        if(!isValidDocumentType(documentType)){
            return ResponseEntity.badRequest().body(null);
        }

        UserData user = new UserData();
        user.setPrimerNombre("Daniel");
        user.setSegundoNombre("Adolfo");
        user.setPrimerApellido("Blanco");
        user.setSegundoApellido("Adrian");
        user.setDireccion("Direccion #123");
        user.setTipoDocumento("P");
        user.setCiudadResidencia("Oruro");
        user.setTelefono("456789");
        user.setNumeroDocumento("112233");

        //Si coincide con el objeto hardcodeado, 200 OK
        if ("P".equals(documentType) && "112233".equals(documentNumber)){
            return ResponseEntity.ok(user);
        }
        //Si no coincide con el objeto hardcodeado, 404 not found
        else{
            return ResponseEntity.notFound().build();
        }
    }

    private boolean isValidDocumentType (String documentType){
        return "C".equals(documentType) || "P".equals(documentType);
    }

    private boolean isValidDocumentNumber(String documentType) {
        // Verificar si documentNumber es un número
        try {
            Long.parseLong(documentType);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
