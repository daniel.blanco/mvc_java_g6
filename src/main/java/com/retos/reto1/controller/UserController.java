package com.retos.reto1.controller;

import com.retos.reto1.service.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.retos.reto1.model.UserData;

@RestController
@RequestMapping("/usuarios")
public class UserController {
    @Autowired
    UserServices userServices;

    @GetMapping(value = "/consultar", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserData> consultarUsuario(
            @RequestParam(name="documentType") String documentType,
            @RequestParam(name="documentNumber") String documentNumber
    ) {

        return userServices.getData(documentType,documentNumber);

    }
}