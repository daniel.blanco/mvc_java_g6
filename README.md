# mvc_java_g6

## Como utilizar el endpoint Spring Boot

Endpoint:
http://localhost:8090/usuarios/consultar

Parámetros:  
*documentType*  
*documentNumber*

## Ejemplos de utilización con respuestas HTTP:

- http://localhost:8090/usuarios/consultar?documentType=P&documentNumber=112233  
*Resultado:200 OK (Es el único usuario hardcodeado con información correcta)*
- http://localhost:8090/usuarios/consultar?documentType=B&documentNumber=2  
*Resultado:400 Bad Request (documentType sólo puede ser las letras C o P)*
- http://localhost:8090/usuarios/consultar?documentType=P&documentNumber=22111  
*Resultado:404 not found* (no existe otro usuario a parte de 112233)
- http://localhost:8090/usuarios/consultar?documentType=1&documentNumber=22111  
*Resultado:500 Internal Server Error* (Cuando se manda documentType como número)